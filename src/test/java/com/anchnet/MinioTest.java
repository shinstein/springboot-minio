package com.anchnet;

import io.minio.*;
import io.minio.errors.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class MinioTest {
    private String bucketName = "zwtest";

    @Autowired
    private MinioClient minioClient;

    /**
     *  查看 zwtest桶 根目录下 中国地图.jpg 文件
     */
    @Test
    public void testGetObject1() throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
        GetObjectResponse response = minioClient.getObject(GetObjectArgs.builder().bucket(bucketName).object("中国地图.jpg").versionId("eef3953b-f3f1-46bc-9372-2a374486f72e").build());

        System.out.println(response.object());
        System.out.println(response.bucket());
        System.out.println(response.region());
    }

    /**
     *  查看 zwtest桶下 test/test.txt 文件
     */
    @Test
    public void testGetObject2() throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
        GetObjectResponse response = minioClient.getObject(GetObjectArgs.builder().bucket(bucketName).object("test/test.txt").build());

        System.out.println(response.object());
        System.out.println(response.bucket());
        System.out.println(response.region());
    }

    /**
     *  查看 zwtest 桶下 test.txt 文件元数据信息
     */
    @Test
    public void testStatObject() throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
        // 不指定版本的情况下默认获取最新版本
//        StatObjectResponse response = minioClient.statObject(StatObjectArgs.builder().bucket(bucketName).object("test.txt").build());

        // 指定版本
        StatObjectResponse response = minioClient.statObject(StatObjectArgs.builder().bucket(bucketName).object("test.txt").versionId("9d13404e-600b-430b-9321-79c1efb1c52e").build());

        Map<String, String> map = response.userMetadata();

        for (Map.Entry<String, String> entry: map.entrySet()) {
            log.info("{} = {}", entry.getKey(), entry.getValue());
        }

        log.info("contentType = {}, deleteMarker = {}, etag = {}, lastModified = {}, size = {}, versionId = {}", response.contentType(),response.deleteMarker(), response.etag(), response.lastModified(), response.size(), response.versionId());
    }

    /**
     * 下载
     */
    @Test
    public void testDownloadObject() throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
        minioClient.downloadObject(DownloadObjectArgs.builder().bucket(bucketName).object("test.txt").filename("C:/Users/bleem/Desktop/test.txt").build());
    }

    /**
     *  将 build.gradle 文件 上传到 bucketName 桶下的 abc/build1.gradle，如果桶下的 abc文件夹不存在，会自动创建
     */
    @Test
    public void testUpload() throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
        minioClient.uploadObject(UploadObjectArgs.builder().bucket(bucketName).filename("build.gradle").contentType("text/plain").object("abc/build1.gradle").build());
    }

    /**
     *  删除文件
     */
    @Test
    public void testDelete1() throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
        minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object("abc/build1.gradle").build());
    }

    /**
     *  删除指定版本的文件
     */
    @Test
    public void testDelete2() throws IOException, InvalidKeyException, InvalidResponseException, InsufficientDataException, NoSuchAlgorithmException, ServerException, InternalException, XmlParserException, ErrorResponseException {
        minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object("中国地图.jpg").versionId("147d95a6-8ba8-4cfa-8eb9-28c8e17af82d").build());
    }
}
